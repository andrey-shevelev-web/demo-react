import React from 'react';

export default class extends React.Component {  
  // constructor(props) {
  //   super(props);
  // }

  state = {
    cnt: 1
  };

  increase = () => {
    this.setState({
      cnt: this.state.cnt + 1
    });
  }

  render() {
    return (
      <div>
        <strong>{this.state.cnt}</strong>
        <br/>
        <button onClick={this.increase}>Add 1</button>
      </div>
    );
  }
}
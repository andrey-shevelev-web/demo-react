import React from  'react';
import ReactDom from  'react-dom';
import App from './app.js';

/*
let hr = React.createElement('hr');
let div = React.createElement('div', {className: 'test'}, [hr]);

ReactDom.render(div, document.querySelector('#app'));
*/

// ReactDom.render(<div className="test">
//   <span>Hello!</span>
//   <hr/></div>, document.querySelector('#app'));

ReactDom.render(<App/>, document.querySelector('#app'));
